namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FeedBacks
    {
        public int Id { get; set; }

        public string PersonName { get; set; }

        public string EmailAddress { get; set; }

        public string Message { get; set; }

        public string Subject { get; set; }

        public string ImprintName { get; set; }
    }
}
