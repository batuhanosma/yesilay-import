namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FileTrackings
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public string FileName { get; set; }
    }
}
