namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RecomendedImprints
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RecomendedImprints()
        {
            ImprintTags = new HashSet<ImprintTags>();
            ImprintTypes = new HashSet<ImprintTypes>();
            ImprintCategories = new HashSet<ImprintCategories>();
        }

        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string SourceLink { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int ImprintStatus { get; set; }

        public string Location { get; set; }

        public string PublishYear { get; set; }

        public string PageCount { get; set; }

        public string JournalName { get; set; }

        public string VolNumber { get; set; }

        public bool? IsInLibrary { get; set; }

        public bool? HasPdf { get; set; }

        public int? PublisherId { get; set; }

        public int? AuthorId { get; set; }

        public int? ImprintLanguageId { get; set; }

        public string PageRange { get; set; }

        public string IssueNumber { get; set; }

        public string PublicationYear { get; set; }

        public string Summary { get; set; }

        public string University { get; set; }

        public string Faculity { get; set; }

        public string Department { get; set; }

        public string ThesisAdvisor { get; set; }

        public string ThesisYear { get; set; }

        public string YokThesisNumber { get; set; }

        public string IBSN { get; set; }

        public string UnitName { get; set; }

        public string FixtureNo { get; set; }

        public string CatalogRecid { get; set; }

        public string CatalogRelation { get; set; }

        public string RecommenderFirstname { get; set; }

        public string RecommenderLastname { get; set; }

        public string RecommenderDescription { get; set; }

        public int ImprintTypeTemp { get; set; }

        public virtual Authors Authors { get; set; }

        public virtual ImprintLanguages ImprintLanguages { get; set; }

        public virtual Publishers Publishers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImprintTags> ImprintTags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImprintTypes> ImprintTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImprintCategories> ImprintCategories { get; set; }
    }
}
