namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RecomendedTerms
    {
        public int Id { get; set; }

        public string EmailAddress { get; set; }

        public string TermName { get; set; }

        public string TermDescription { get; set; }

        public string ScientificResourceLink { get; set; }

        public int TermCategory { get; set; }

        public string CustomCategory { get; set; }

        public int TermStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public DateTime? DeleteDate { get; set; }

        public string RecommenderFirstname { get; set; }

        public string RecommenderLastname { get; set; }
    }
}
