namespace Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class YesilayTbaContext : DbContext
    {
        public YesilayTbaContext()
            : base("name=YesilayTbaContext")
        {
        }

        public virtual DbSet<Authors> Authors { get; set; }
        public virtual DbSet<FeedBacks> FeedBacks { get; set; }
        public virtual DbSet<FileTrackings> FileTrackings { get; set; }
        public virtual DbSet<ImprintCategories> ImprintCategories { get; set; }
        public virtual DbSet<ImprintLanguages> ImprintLanguages { get; set; }
        public virtual DbSet<ImprintTags> ImprintTags { get; set; }
        public virtual DbSet<ImprintTypes> ImprintTypes { get; set; }
        public virtual DbSet<LinkTrackings> LinkTrackings { get; set; }
        public virtual DbSet<Publishers> Publishers { get; set; }
        public virtual DbSet<RecomendedImprints> RecomendedImprints { get; set; }
        public virtual DbSet<RecomendedTerms> RecomendedTerms { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<SearchTrackings> SearchTrackings { get; set; }
        public virtual DbSet<UserClaims> UserClaims { get; set; }
        public virtual DbSet<UserLogins> UserLogins { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Authors>()
                .HasMany(e => e.RecomendedImprints)
                .WithOptional(e => e.Authors)
                .HasForeignKey(e => e.AuthorId);

            modelBuilder.Entity<ImprintCategories>()
                .HasMany(e => e.RecomendedImprints)
                .WithMany(e => e.ImprintCategories)
                .Map(m => m.ToTable("RecomendedImprintImprintCategories").MapRightKey("RecomendedImprint_Id"));

            modelBuilder.Entity<ImprintLanguages>()
                .HasMany(e => e.RecomendedImprints)
                .WithOptional(e => e.ImprintLanguages)
                .HasForeignKey(e => e.ImprintLanguageId);

            modelBuilder.Entity<ImprintTags>()
                .HasMany(e => e.RecomendedImprints)
                .WithMany(e => e.ImprintTags)
                .Map(m => m.ToTable("ImprintTagsRecomendedImprints").MapRightKey("RecomendedImprint_Id"));

            modelBuilder.Entity<ImprintTypes>()
                .HasMany(e => e.RecomendedImprints)
                .WithMany(e => e.ImprintTypes)
                .Map(m => m.ToTable("ImprintTypeRecomendedImprints").MapLeftKey("ImprintType_Id").MapRightKey("RecomendedImprint_Id"));

            modelBuilder.Entity<Publishers>()
                .HasMany(e => e.RecomendedImprints)
                .WithOptional(e => e.Publishers)
                .HasForeignKey(e => e.PublisherId);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("UserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<Users>()
                .HasMany(e => e.UserClaims)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.UserLogins)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.UserId);
        }
    }
}
