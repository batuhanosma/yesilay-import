﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YesilayDataImport
{
    public class ImprintTypeRepository
    {
        private readonly YesilayTbaContext _context;

        public ImprintTypeRepository(YesilayTbaContext context)
        {
            _context = context;
        }

        public ImprintTypes GetImprintTypeByName(string name)
        {
            var imprntType = _context.ImprintTypes.FirstOrDefault(x => x.Name == name);

            if(imprntType == null)
            {
                imprntType = _context.ImprintTypes.FirstOrDefault(x => x.Id == 4); // diğer 
            }

            return imprntType;
        }

    }
}
