﻿using ClosedXML.Excel;
using Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Transactions;
//using System;

namespace YesilayDataImport
{
    class Program
    {
        static string Aktif_File_Path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"aktif.xlsx");
        static string Pasif_File_Path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"pasif.xlsx");


        //const string File_Path = "C:\\aktif.xlsx";

        static void Main(string[] args)
        {
            try
            {
                ReadExcel(Aktif_File_Path, 2);

                Console.WriteLine("************************************** Aktif Finished *********************************************************");

                ReadExcel(Pasif_File_Path, 1);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message + "Inner: " + ex?.InnerException?.Message);
                Console.ReadLine();
            }
        }

        private static string FixCategoryName(string categoryName)
        {
            if(categoryName == "Uyuşturucu Bağımlılığı")
            {
                categoryName = "Madde Bağımlılığı";
            }
            else if (categoryName == "Uçucu Madde Bağımlılığı")
            {
                categoryName = "Madde Bağımlılığı";
            }
            else if (categoryName == "Medikal Hap Bağımlılığı")
            {
                categoryName = "İlacın Kötüye Kullanımı";
            }
            else if (categoryName == "Sigara Bağımlılığı")
            {
                categoryName = "Tütün Bağımlılığı";
            }
            else if (categoryName == "Cinsel Bağımlılık")
            {
                categoryName = "Davranışsal Bağımlılık";
            }
            else if (categoryName == "Alışveriş Bağımlılığı")
            {
                categoryName = "Davranışsal Bağımlılık";
            }
            else if (categoryName == "Cep Telefonu Bağımlılığı")
            {
                categoryName = "Akıllı Telefon Kullanımı";
            }
            else if (categoryName == "Yeme-İçme Bağımlılığı")
            {
                categoryName = "Davranışsal Bağımlılık";
            }

            return categoryName;
        }

        static void ReadExcel(string path, int imprintStatus)
        {
            var dbContext = new YesilayTbaContext();
            var languageRepository = new LanguageRepository(dbContext);
            var authorRepository = new AuthorRepository(dbContext);
            var publisherRepository = new PublisherRepository(dbContext);
            var categoryRepository = new CategoryRepository(dbContext);
            var tagRepository = new TagRepository(dbContext);
            var imprintTypeRepository = new ImprintTypeRepository(dbContext);

            int i = 2;
            var workbook = new XLWorkbook(path);
            var ws1 = workbook.Worksheet(1);

            var recomendedImprintList = new List<RecomendedImprints>();
            var languages = new List<ImprintLanguages>();

            do
            {
                var row = ws1.Row(i);

                if (row.IsEmpty())
                {
                    break;
                }

                //var imprintTypeStr = row.Cell(3)?.Value?.ToString();
                //ImprintTypes imprintTypeValue;

                //if(imprintTypeStr == "Kitap")
                //{
                //    imprintTypeValue = ImprintTypes.Book;
                //}
                //else if(imprintTypeStr == "Kitap Bölümü")
                //{
                //    imprintTypeValue = ImprintTypes.BookPart;
                //}
                //else if (imprintTypeStr == "Makale")
                //{
                //    imprintTypeValue = ImprintTypes.Article;
                //}
                //else if(imprintTypeStr == "Tez")
                //{
                //    imprintTypeValue = ImprintTypes.Thesis;
                //}
                //else
                //{
                //    imprintTypeValue = ImprintTypes.Other;
                //}

                var language = row.Cell(5)?.Value?.ToString();
                var languageObj = languageRepository.GetByName(language);
                if(languageObj == null)
                {
                    languageRepository.Create(new ImprintLanguages() { LanguageName = language });
                    languageObj = languageRepository.GetByName(language);
                }

                var authorStr = row.Cell(6)?.Value?.ToString();
                Authors author = authorRepository.GetByName(authorStr);
                if(author == null)
                {
                    authorRepository.Create(new Authors() { FullName = authorStr});
                    author = authorRepository.GetByName(authorStr);
                }

                var publisherStr = row.Cell(7)?.Value?.ToString();
                Publishers publisher = publisherRepository.GetByName(publisherStr);
                if(publisher == null)
                {
                    publisherRepository.Create(new Publishers() { Name = publisherStr});
                    publisher = publisherRepository.GetByName(publisherStr);
                }

                var publicationYearStr = row.Cell(8)?.Value?.ToString();

                var pageCountStr = row.Cell(10)?.Value?.ToString();

                var isInLibraryStr = row.Cell(13)?.Value?.ToString();
                bool? inLibrary = null;
                if(isInLibraryStr?.ToLower() == "evet")
                {
                    inLibrary = true;
                }else if (isInLibraryStr?.ToLower() == "hayır")
                {
                    inLibrary = false;
                }

                var hasPdfStr = row.Cell(14)?.Value?.ToString();
                bool? hasPdf = null;
                if(hasPdfStr?.ToLower() == "var")
                {
                    hasPdf = true;
                }

                var volNumberStr = row.Cell(18)?.Value?.ToString();

                var issueNumberStr = row.Cell(19)?.Value?.ToString();

                var publishYearStr = row.Cell(20)?.Value?.ToString();

                var thesisYearStr = row.Cell(26)?.Value?.ToString();

                var recomendedImprint = new RecomendedImprints()
                {
                    FullName = row.Cell(2)?.Value?.ToString(),
                    ImprintLanguageId = languageObj.Id,
                    AuthorId = author.Id,
                    PublisherId = publisher.Id,
                    PublicationYear = publicationYearStr,
                    Location = row.Cell(9)?.Value?.ToString(),
                    PageCount = pageCountStr,
                    IsInLibrary = inLibrary,
                    HasPdf = hasPdf,
                    SourceLink = row.Cell(15)?.Value?.ToString(),
                    JournalName = row.Cell(16)?.Value?.ToString(),
                    PageRange = row.Cell(17)?.Value?.ToString(),
                    VolNumber = volNumberStr,
                    IssueNumber = issueNumberStr,
                    PublishYear = publishYearStr,
                    Summary = row.Cell(21)?.Value?.ToString(),
                    University = row.Cell(22)?.Value?.ToString(),
                    Faculity = row.Cell(23)?.Value?.ToString(),
                    Department = row.Cell(24)?.Value?.ToString(),
                    ThesisAdvisor = row.Cell(25)?.Value?.ToString(),
                    ThesisYear = thesisYearStr,
                    YokThesisNumber = row.Cell(27)?.Value?.ToString(),
                    IBSN = row.Cell(29)?.Value?.ToString(),
                    UnitName = row.Cell(35)?.Value?.ToString(),
                    FixtureNo = row.Cell(36)?.Value?.ToString(),
                    CatalogRecid = row.Cell(38)?.Value?.ToString(),
                    CatalogRelation = row.Cell(39)?.Value?.ToString(),
                    CreatedDate = DateTime.Now,
                    ImprintStatus = imprintStatus
                };

                var imprintCategories = new List<ImprintCategories>();
                var imprintTags = new List<ImprintTags>();
                var imprintTypes = new List<ImprintTypes>();


                //TODO: KATEGORİ, ETİKET
                var categoriesStr = Regex.Replace(row.Cell(11)?.Value?.ToString(), @"\t|\n|\r", ""); 
                var categoryList = categoriesStr.Split(',').ToList();

                if (categoryList.Contains("İnternet Bağımlılığı"))
                {
                    categoryList.Remove("İnternet Bağımlılığı");

                    categoryList.Add("Davranışsal Bağımlılık");
                    categoryList.Add("Sosyal Medya Kullanımı");
                }

                if (categoryList.Contains("Kumar-Bahis Bağımlılığı"))
                {
                    categoryList.Remove("Kumar-Bahis Bağımlılığı");

                    categoryList.Add("Kumar Bağımlılığı");
                    categoryList.Add("Patolojik Kumar Oynama");
                }

                foreach (var category in categoryList)
                {
                    if(string.IsNullOrEmpty(category))
                    {
                        continue;
                    }

                    if(category == "Bilgisayar Bağımlılığı" || category == "Televizyon Bağımlılığı" || category == "Bilgisayar Bağımlılığı"
                        || category == "İş Bağımlılığı")
                    {
                        continue;
                    }

                    var categoryName = FixCategoryName(category);

                    var categoryDb = categoryRepository.GetByName(categoryName);
                    if(categoryDb == null)
                    {
                        var newCategoryToCreate = new ImprintCategories();
                        newCategoryToCreate.Name = categoryName;
                        categoryRepository.Create(newCategoryToCreate);

                        newCategoryToCreate = categoryRepository.GetByName(categoryName);

                        imprintCategories.Add(newCategoryToCreate);
                    }
                    else
                    {
                        imprintCategories.Add(categoryDb);
                    }
                }

                var tagStr = row.Cell(12)?.Value?.ToString();
                var tagList = tagStr.Split('\n');

                for (int j = 0; j < tagList.Length; j++)
                {
                    if (string.IsNullOrEmpty(tagList[j]))
                    {
                        continue;
                    }

                    tagList[j] = Regex.Replace(tagList[j], @"\t|\n|\r", "");

                    var tag = tagRepository.GetByName(tagList[j]);
                    if (tag == null)
                    {
                        var newTagToCreate = new ImprintTags();
                        newTagToCreate.Name = tagList[j];
                        tagRepository.Create(newTagToCreate);

                        newTagToCreate = tagRepository.GetByName(tagList[j]);

                        imprintTags.Add(newTagToCreate);
                    }
                    else
                    {
                        imprintTags.Add(tag);
                    }
                }

                var imprintTypeStr = Regex.Replace(row.Cell(3)?.Value?.ToString(), @"\t|\n|\r", "");

                var imprintType = imprintTypeRepository.GetImprintTypeByName(imprintTypeStr);

                imprintTypes.Add(imprintType);
                
                //var imprintType = getByName

                recomendedImprint.ImprintTags = imprintTags;
                recomendedImprint.ImprintCategories = imprintCategories;
                recomendedImprint.ImprintTypes = imprintTypes;

                recomendedImprintList.Add(recomendedImprint);

                Console.WriteLine($"{i}. kayit eklendi");

                i++;

                if(i == 7000)
                {
                    break;
                }

            } while (true);

            Console.WriteLine("Database e yükleme basladi************************************************************");
            BulkInsert(recomendedImprintList, dbContext);
        }



        //https://stackoverflow.com/a/5942176/7293218
        static void BulkInsert(List<RecomendedImprints> recomendedImprints, YesilayTbaContext context)
        {
            using (TransactionScope scope =
             new TransactionScope(TransactionScopeOption.Required,
                                   new System.TimeSpan(0,59, 0)))
            {
                //YesilayTbaEntities context = null;
                try
                {
                    //context = new YesilayTbaEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;

                    int count = 0;
                    foreach (var entityToInsert in recomendedImprints)
                    {
                        ++count;
                        context = AddToContext(context, entityToInsert, count, 100, false);
                    }

                    context.SaveChanges();
                    Console.WriteLine("Tüm kayitlar db ye eklendi");
                }
                finally
                {
                    if (context != null)
                        context.Dispose();
                }

                scope.Complete();
            }
        }



        private static YesilayTbaContext AddToContext(YesilayTbaContext context,
    RecomendedImprints entity, int count, int commitCount, bool recreateContext)
        {
            context.Set<RecomendedImprints>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new YesilayTbaContext();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
                Console.WriteLine("100 kayit context e eklendi");
            }

            return context;
        }

        //    enum ImprintTypes
        //    {
        //        Article,
        //        Book,
        //        Thesis,
        //        BookPart,
        //        Other
        //    }
    }
}
